import {createStore} from 'vuex';

export default createStore({
    state: {
        user: null,
        editingUser: null,
        isAuthenticated: false,
        loginError: true,

    },
    mutations: {
        async loginUser(state, user) {
            console.log(user);
            const url = `http://localhost:3001/api/users?email=${user.email}`;
            const response = await fetch(url);
            const jsonData = await response.json();
            if (user.password === jsonData.password && user.email === jsonData.email)
            {
                state.user = user;
                state.isAuthenticated = true;
                state.loginError = false;
                alert("Login success");
            }
            else {
                state.isAuthenticated = false;
                state.loginError = true;
                alert("Invalid information");
            }
        },
        async logoutUser(state) {
            state.user = null;
            state.isAuthenticated = false;
        },
        async saveEditedUser(state, editingUser) {
            const response = await fetch('http://localhost:3001/api/users/', {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(editingUser),
            });

            const result = await response.json();
            if (result.result === "success") {
                alert('Update Success');
            } else {
                alert('Edit Error');
            }
        },
    },
    actions: {
        login({commit}, user) {
            commit('loginUser', user);
        },
        logout({commit}) {
            commit('logoutUser');
        },
        saveEdited({commit}, editingUser) {
            commit('saveEditedUser', editingUser);
        },
    },
    getters: {
        isAuthenticated: state => state.isAuthenticated,
        editingUser: state => state.editingUser,
        loginError: state => state.loginError,
    },
})