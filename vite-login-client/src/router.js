import * as VueRouter from 'vue-router'
import LoginForm from "./components/LoginForm.vue";
import EditForm from "./components/EditForm.vue";

const routes = [
    {path: "/login", component: LoginForm},
    {path: "/edit", component: EditForm},
]

const router = VueRouter.createRouter({
    history: VueRouter.createWebHistory(),
    routes,
})

export default router