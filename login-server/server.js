// server.js
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const port = 3001;
const mongoose= require('mongoose');

mongoose.connect('mongodb://127.0.0.1:27017/user');
const LoginModel = mongoose.model('login', new mongoose.Schema({
    username: String,
    password: String
}));

app.use(bodyParser.json());
app.use(cors());

app.get('/api/users', async (req, res) => {
    const user
        = await LoginModel.findOne({username: req.query.email});
    res.json(user);
});

app.put('/api/users/', async (req, res) => {
    const filter = { username: req.body.email };
    const update = { password: req.body.password };

    const doc = await LoginModel.findOneAndUpdate(filter, update, {
        new: true
    });
    if (doc != null)
    {
        res.json({
            "result": "success",
            "data": doc,
        });
    }
    else
    {
        res.json({
            "result": "fail",
            "data": null,
        })
    }
});

app.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
});
